using System;
using System.Collections.Generic;
using Xunit;

namespace TechnoB.Test
{
    public class UnitTest1
    {
        private Class1 _cal;
        [Fact]
        public void Test1()
        {

        }
        [Fact]
        public string check()
        {
            List<string> csv = new List<string>(new string[] { "10:20:500", "11:20:500", "12:20:500", "14::500" });

            List<string> employees = new List<string>(new string[] { "10: Employee2", "20: Employee3", "12: Employee4", "13: Employee5", "14: Employee5" });
            //Arrange
            var emp = _cal.Employee(csv, employees);
            //var postId = 2;

            //Act
            string data = _cal.Employee(csv, employees);

            //Assert
            return data;
        }
        public string checkCEO()
        {

            List<string> employees = new List<string>(new string[] { "10: Employee2", "20: Employee3", "12: Employee4", "13: Employee5", "14: Employee5" });
            //Arrange
            var emp = _cal.checkCEO(employees);
            //var postId = 2;

            //Act
            string data = _cal.checkCEO(employees);

            //Assert
            return data;
        }
        public string checkCircular()
        {
            List<string> csv = new List<string>(new string[] { "10:20:500", "11:20:500", "12:20:500", "14::500" });
            //Arrange
            var emp = _cal.checkCircular(csv);
            //var postId = 2;

            //Act
            string data = _cal.checkCircular(csv);

            //Assert
            return data;
        }
        public bool checkManage()
        {
            List<string> csv = new List<string>(new string[] { "10:20:500", "11:20:500", "12:20:500", "14::500" });
            List<string> employees = new List<string>(new string[] { "10: Employee2", "20: Employee3", "12: Employee4", "13: Employee5", "14: Employee5" });
            //Arrange
            var emp = _cal.checkManage(csv, employees);
            //var postId = 2;

            //Act
            bool data = _cal.checkManage(csv, employees);

            //Assert
            return data;
        }
        public string checkManages()
        {
            List<string> csv = new List<string>(new string[] { "10:20:500", "11:20:500", "12:20:500", "14::500" });
            List<string> employees = new List<string>(new string[] { "10: Employee2", "20: Employee3", "12: Employee4", "13: Employee5", "14: Employee5" });
            //Arrange
            var emp = _cal.checkManages(csv);
            //var postId = 2;

            //Act
            string data = _cal.checkManages(csv);

            //Assert
            return data;
        }
    }
}
