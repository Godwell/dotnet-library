﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace TechnoB
{
    public class Class1
    {
        public string Employee(List<string> csv, List<string> employees)
        {
                
            // 1. check if salary is an int
            string message1 = "";
            if (csv != null)
            {
                foreach (var sampleEmp in csv)
                {
                    string emp1 = sampleEmp;

                    var empId = emp1.Split(',')[0];

                    var salary = emp1.Split(',')[2];

                    bool status = checkSalary(salary);

                    if(status == true)
                    {
                        message1 = " " + emp1 + "salary is an int";
                    }
                    else
                    {
                        message1 = " " + emp1 + "salary is an not int";
                    }
                    
                }
            }

            return message1;
        }
        // Regex to check if its bumbers only
        public bool checkSalary(string salary)
        {
            Regex regex = new Regex(@"^\d$");

            if (regex.IsMatch(salary))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        // Check CEO
        public string checkCEO(List<string> employees)
        {
            string message = "";

            int ceo = 0;

            if (employees != null)
            {
                foreach (var sampleEmp in employees)
                {
                    string emp1 = sampleEmp;

                    var empId = emp1.Split(',');
                    int y = empId.Length;

                    if (y == 3)
                    {
                        ceo = +1;
                    }
                }
            }
            if (ceo > 1)
            {
                message = "Error: There can be only One CEO in the Organisation";
            }
            else
            {
                message = "Successfull: There is only one CEO";
            }
            return message;
        }
        //check Circular
        public string checkCircular(List<string> employees)
        {
            string message = "";
            if (employees != null)
            {
                foreach (var sampleEmp in employees)
                {
                    string emp1 = sampleEmp;

                    var empId = emp1.Split(':')[0];

                    string manId = emp1.Split(':')[1];

                    var lobj_Result = employees.Where(o => o.Contains(manId));

                    foreach (var y in lobj_Result)
                    {
                        var empId1 = y.Split(':')[0];

                        var manId1 = y.Split(':')[1];

                        if (empId1 == manId && manId1 == empId)
                        {
                            message = "Error";
                        }
                        else
                        {
                            message = "success";
                        }
                    }
                }
            }
            return message;
        }
        // check if a manager is an employee
        public bool checkManage(List<string> csv, List<string> employeeList)
        {

            foreach (var sampleEmp in csv)
            {
                string emp1 = sampleEmp;

                var manId = emp1.Split(':')[1];

                var salary = emp1.Split(':')[2];

                var match = employeeList.Where(o => o.Contains(manId));

                if (match != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
                

            }
            return false;
        }

        //add sal of managers
        public string checkManages(List<string> csv)
        {
            string mannId = "20";
            long x = 0;
            foreach (var sampleEmp in csv)
            {
                string emp1 = sampleEmp;

                var empId = emp1.Split(':')[0];

                string manId = emp1.Split(':')[1];

                string sall = emp1.Split(':')[2];

                long sal = Convert.ToInt64(sall);

                if (mannId == manId)
                {
                    x = x + sal;
                }
            }
            return mannId + "Result: " + x;
        }
    }
}
